# csrf和xss过滤器demo

#### 介绍
 
~~~
demo    
├── config              
│       └── XSSFilterAndAuthFilterConfig              // 过滤器配置
├── filters                                           // 过滤器目录
│       └── SignAuthFilter                            // 签名验证过滤器
│       └── SignAuthServletRequestWrapper             // 签名验证 request wrapper
│       └── XssFilter                                 // xss过滤器
│       └── XssHttpServletRequestWrapper              // xss过滤器 request wrapper
├── util                                              // 工具类
├── resources                                         // 资源目录
│       └── fornt-demo                                // 前端GET和POST请求demo
│       └── postman-demo                              // Postman请求脚本demo
├──pom.xml                                            //  依赖
~~~

 
#### 一、前后端使用说明

1.  后端开启过滤器xss过滤器和signAuth过滤器
2.  signAuth过滤器需要前端在每个请求之前，把参数按照规则拼接生成签名
3.  后端在过滤器里，按照同样的规则生成sign并且比对是否一致(小写字母)
 
#### 二、postman脚本使用说明

说明:
在请求接口前，把参数按照key字母排序，加上时间戳和Secret做md5生成sign，在请求头里设置Timestamp和Sign

使用方法
1. 单击 "xx Collection" 或 "xx Request"
2. 点击右侧 "Pre-request Script"，复制粘贴脚本
 