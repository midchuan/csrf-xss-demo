package com.example.demo.model;

import lombok.Data;

@Data
public class MockParams {

    private String a;

    private String b;

    private String g;

}
