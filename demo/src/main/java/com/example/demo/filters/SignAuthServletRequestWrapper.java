package com.example.demo.filters;

import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.http.HtmlUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.TypeReference;
import com.example.demo.util.HttpServletUtil;
import com.example.demo.util.StringUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@Slf4j
public class SignAuthServletRequestWrapper extends HttpServletRequestWrapper {

    private HttpServletResponse response;
    private HttpServletRequest request;
    private Boolean isRequireSign;
    private Boolean isHandle =false;

    private static final String ENCRYPT_SECRET = "350cb4303db847788eee5b125f7fdbaa";

    private static final String METHOD_GET = "GET";

    private static final String METHOD_POST = "POST";

    public SignAuthServletRequestWrapper(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Boolean isRequireSign) {
        super(httpServletRequest);
        response = httpServletResponse;
        request = httpServletRequest;
        this.isRequireSign = true;
    }

    public SignAuthServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getHeader(String name) {
        String value = super.getHeader(name);
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return HtmlUtil.filter(value);
    }

    @Override
    public String getParameter(String name) {
        String value = super.getParameter(name);
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return  HtmlUtil.filter(value);
    }

    @SneakyThrows
    @Override
    public String[] getParameterValues(String name) {

        String method = super.getMethod();
        log.info("getParameterValues");
        log.info(method);
        String contentType = request.getContentType();
        SortedMap<String, String> paramsMap = null;
        paramsMap = (SortedMap<String, String>) HttpServletUtil.getParameter(request);
        // 请求方式为GET或者为POST的application/x-www-form-urlencoded
        if( isRequireSign && !isHandle &&  (METHOD_GET.equals(method) || contentType.contains(CONTENT_TYPE_X_WWW_FORM_URLENCODED))) {

            paramsMap = (SortedMap<String, String>) HttpServletUtil.getParameter(request);

            if (paramsMap == null) {
                paramsMap = new TreeMap<>();
            }

            // 获取签名和时间戳
            String sign = super.getHeader("Sign");
            String timestampStr = super.getHeader("Timestamp");

            if (StringUtils.isBlank(sign)) {
                sign = paramsMap.get("Sign");
                paramsMap.remove("Sign");
            }

            if (StringUtils.isBlank(timestampStr)) {
                timestampStr = paramsMap.get("Timestamp");
                paramsMap.remove("Timestamp");
            }

            if (StringUtils.isEmpty(sign)) {
                throw new Exception("签名不能为空");
            }
            if (StringUtils.isEmpty(timestampStr)) {
                throw new Exception("时间戳不能为空");
            }
            // 重放时间限制
            long timestamp = Long.parseLong(timestampStr);
            if (System.currentTimeMillis() - timestamp >= timeoutMs) {
                throw new Exception("签名已过期");
            }

            Boolean verifyRes = verifySign(paramsMap, sign, timestampStr);
            if (!verifyRes && isRequireSign) {
                throw new Exception("签名校验失败");
            }
            isHandle = true;
        }

        String[] values = super.getParameterValues(name);
        if (values == null) {
            return null;
        }
        for (int i = 0; i < values.length; i++) {
            if (StringUtils.isEmpty(values[i])) {
                values[i] = values[i];
            } else {
                values[i] = HtmlUtil.filter(values[i]);
            }
        }
        return values;
    }

    private static final String CONTENT_TYPE_UPLOAD_FILE = "multipart/form-data";
    private static final String CONTENT_TYPE_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    private static final String CONTENT_FORM_DATA = "multipart/form-data";

    private static final Integer timeoutMs =60*1000;

    @SneakyThrows
    @Override
    public ServletInputStream getInputStream() throws IOException {

        String method = super.getMethod();
        log.info("getInputStream");
        log.info(method);

        String contentType = this.getRequest().getContentType();
        log.info(" content type :" +this.getRequest().getContentType());

        // 判断是否为文件上传，如果为文件上传不进行判断
        if (this.getRequest().getContentType().contains(CONTENT_TYPE_UPLOAD_FILE)) {
            return super.getInputStream();
        } else {
            return getInputStreamWithFilter();
        }
    }

    public static Boolean isObjectOrArray(String str) {
        if (str == null) {
            return false;
        }
        return (str.startsWith("[") && str.endsWith("]")) || (str.startsWith("{") && str.endsWith("}"));
    }

    public static void removeObjectOrArrayItem(Map<String,String> map) {
        Iterator iter = map.entrySet().iterator();
        String curKey;
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();
            if (key != null) {
                // curKey = key.toString();
                String value = (String) entry.getValue();
                if (isObjectOrArray(value)) {
                    iter.remove();
                }
            }
        }
    }

    private static Boolean isArrayStr(String str) {
        if (str == null) {
            return false;
        }

        return str.startsWith("[") && str.endsWith("]");
    }

    private static Boolean isObjectStr(String str)
    {
        if (str == null) {
            return false;
        }

        return str.startsWith("{") && str.endsWith("}");
    }

    public ServletInputStream getInputStreamWithFilter() throws Exception {

        InputStream in = super.getInputStream();
        StringBuffer body = new StringBuffer();
        InputStreamReader reader = new InputStreamReader(in, Charset.forName("UTF-8"));
        BufferedReader buffer = new BufferedReader(reader);
        String line = buffer.readLine();
        while (line != null) {
            body.append(line);
            line = buffer.readLine();
        }
        buffer.close();
        reader.close();
        in.close();

        String json = body.toString();
        Boolean isArray = isArrayStr(json);
        if(isRequireSign && !isHandle ) {
            if (isArray) {
                arrayVerifySignHandler(json);
            } else {
                objectVerifySignHandler(json);
            }
        }

        final ByteArrayInputStream bain = new ByteArrayInputStream(json.getBytes());
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return bain.read();
            }
            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener listener) {
            }
        };
    }

    public void objectVerifySignHandler(String json) throws Exception {
        // 获取签名和时间戳
        String sign = super.getHeader("Sign");
        String timestampStr = super.getHeader("Timestamp");
        String method = super.getMethod();

        SortedMap<String, String> paramsMap = paramsMap = JSON.parseObject(json, new TypeReference<SortedMap<String, String>>() {
        });

        if (paramsMap == null) {
            paramsMap = new TreeMap<>();
        }

        if (StringUtils.isBlank(sign)) {
            sign = paramsMap.get("Sign");
            paramsMap.remove("Sign");
        }

        if (StringUtils.isBlank(timestampStr)) {
            timestampStr = paramsMap.get("Timestamp");
            paramsMap.remove("Timestamp");
        }

        if (StringUtils.isEmpty(sign)) {
            throw new Exception("签名不能为空");
        }
        if (StringUtils.isEmpty(timestampStr)) {
            throw new Exception("时间戳不能为空");
        }

        // 重放时间限制
        long timestamp = Long.parseLong(timestampStr);
        if (System.currentTimeMillis() - timestamp >= timeoutMs) {
            throw new Exception("签名已过期");
        }

        Boolean verifyRes = verifySign(paramsMap, sign, timestampStr);
        if (!verifyRes && isRequireSign) {
            throw new Exception("签名校验失败");
        }
        isHandle = true;
    }

    public void arrayVerifySignHandler(String json) throws Exception {
        JSONArray objects = JSON.parseArray(json);
        log.error("objects:{}",JSON.toJSONString(objects));
        // 获取签名和时间戳
        String sign = super.getHeader("Sign");
        String timestampStr = super.getHeader("Timestamp");
        String method = super.getMethod();

        if (StringUtils.isEmpty(sign)) {
            throw new Exception("签名不能为空");
        }
        if (StringUtils.isEmpty(timestampStr)) {
            throw new Exception("时间戳不能为空");
        }

        // 重放时间限制
        long timestamp = Long.parseLong(timestampStr);
        if (System.currentTimeMillis() - timestamp >= timeoutMs) {
            throw new Exception("签名已过期");
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(Integer i = 0 ;i< objects.size();i++)
        {
            Object item = objects.get(i);
            stringBuilder.append(String.format("%d=%s&", i, JSON.toJSONString(item)));
        }

        stringBuilder.append("timestamp="+timestampStr+"&");
        stringBuilder.append("secret=" + ENCRYPT_SECRET);
        String paramsStr = stringBuilder.toString();
        String md5 = DigestUtil.md5Hex(paramsStr).toLowerCase();
        Boolean verifyRes = md5.equals(sign);

        // Boolean verifyRes = verifySign(json, sign, timestampStr);
        if (!verifyRes && isRequireSign) {
            throw new Exception("签名校验失败");
        }
        isHandle = true;
    }

    /**
     * 验证签名
     * 说明:如果是数组json的参数，直接字符串拼接时间戳和秘钥做md5
     * @param arrayJsonStr
     * @param sign
     * @param timestampStr
     * @return
     */
    public static Boolean verifySign(String arrayJsonStr,String sign,String timestampStr) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(arrayJsonStr);
        stringBuilder.append("timestamp="+timestampStr+"&");
        stringBuilder.append("secret=" + ENCRYPT_SECRET);
        String paramsStr = stringBuilder.toString();
        String md5 = DigestUtil.md5Hex(paramsStr).toLowerCase();
        return md5.equals(sign);
    }


    public static Boolean verifySign(SortedMap<String, String> map,String sign,String timestampStr) {

        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            stringBuilder.append(String.format("%s=%s&", entry.getKey(), entry.getValue()));
        }
        stringBuilder.append("timestamp="+timestampStr+"&");
        stringBuilder.append("secret=" + ENCRYPT_SECRET);
//        stringBuilder.substring(0, stringBuilder.length() - 1);
        String paramsStr = stringBuilder.toString();
        String md5 = DigestUtil.md5Hex(paramsStr).toLowerCase();
        return md5.equals(sign);
    }
}
