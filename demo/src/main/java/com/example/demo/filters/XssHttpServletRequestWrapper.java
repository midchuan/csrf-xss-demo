package com.example.demo.filters;

import cn.hutool.http.HtmlUtil;
import com.example.demo.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.nio.charset.Charset;

@Slf4j
public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {


    public XssHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getHeader(String name) {
        String value = super.getHeader(name);
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return HtmlUtil.filter(value);
    }

    @Override
    public String getParameter(String name) {
        String value = super.getParameter(name);
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return  HtmlUtil.filter(value);
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] values = super.getParameterValues(name);
        if(values == null) {
            return null;
        }
        for (int i = 0; i < values.length; i++) {
            if (StringUtils.isEmpty(values[i])) {
                values[i] = values[i];
            } else {
                values[i] = HtmlUtil.filter(values[i]);
            }
        }
        return values;
    }

    private static final String CONTENT_TYPE_UPLOAD_FILE = "multipart/form-data";



    @Override
    public ServletInputStream getInputStream() throws IOException {
        String contentType = this.getRequest().getContentType();
        log.info(" content type :" +this.getRequest().getContentType());
//        return super.getInputStream();
        if (this.getRequest().getContentType().contains(CONTENT_TYPE_UPLOAD_FILE)) {
            return super.getInputStream();
        } else {
            return getInputStreamWithXSSFilter();
        }

    }


    public ServletInputStream getInputStreamWithXSSFilter() throws IOException {

        InputStream in = super.getInputStream();
        StringBuffer body = new StringBuffer();
        InputStreamReader reader = new InputStreamReader(in, Charset.forName("UTF-8"));
        BufferedReader buffer = new BufferedReader(reader);
        String line = buffer.readLine();
        while (line != null) {
            body.append(line);
            line = buffer.readLine();
        }
        buffer.close();
        reader.close();
        in.close();

        String str = HtmlUtil.filter(body.toString());
        str = str.replace("&quot;","\"");
        final ByteArrayInputStream bain = new ByteArrayInputStream(str.getBytes());
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return bain.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener listener) {
            }
        };
    }
}
