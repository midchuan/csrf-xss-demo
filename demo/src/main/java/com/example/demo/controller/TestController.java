package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.example.demo.model.MockParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class TestController {


//    public String testGetNoParams()
//    {
//        return "ok";
//    }

    @GetMapping("/testGet")
    @ResponseBody
    public String testGet(String a,String b) {
        return "ok";
    }

    @PostMapping("/testPostJson")
    @ResponseBody
    public String testPostJson(@RequestBody MockParams params)
    {
        return "ok";
    }

    @PostMapping("/testPostJsonList")
    @ResponseBody
    public String testPostJsonList(@RequestBody List<MockParams> list) {
        log.info("json :{}", JSON.toJSONString(list));
        return "ok";
    }

    @PostMapping("/testPostForm")
    @ResponseBody
    public String testPostForm(MockParams params)
    {
        return "ok";
    }

}
